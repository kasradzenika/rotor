﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class MainMenuControlelr : MonoBehaviour {

    public UnityEvent didActivate;
    public UnityEvent didDeactivate;

	void Start () {
        
	}

    //Menu lifecycle
    public void Activate(){
        Invoke("DidActivate", 0.5f);
    }

    public void Deactivate(){
        Invoke("DidDeactivate", 0.5f);
    }

    public void DidActivate(){
        didActivate.Invoke();
    }

    public void DidDeactivate(){
        didDeactivate.Invoke();
    }

    //OnClick listeners:
    public void OnClickPlay(){
        //TODO
    }

    public void OnClickMute(){
        int muteIsOn = PlayerPrefs.GetInt("MuteStatus", 0); //get value
        muteIsOn = (muteIsOn == 0) ? 1 : 0; //change value

        AudioListener.volume = muteIsOn; //apply value
        PlayerPrefs.SetInt("MuteStatus", muteIsOn); //save
    }

    public void OnClickLeaderboard(){
        //TODO
    }
}
