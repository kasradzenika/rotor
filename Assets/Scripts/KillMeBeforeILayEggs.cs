﻿using UnityEngine;
using System.Collections;

public class KillMeBeforeILayEggs : MonoBehaviour {

    public float aliveTime = 1f;

	void Start () {
        Invoke("KillMe", aliveTime);
	}
	
    void KillMe(){
        Destroy(gameObject);
    }
}
