﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class InputSensor : MonoBehaviour {

	public UnityEvent doAction;
	public UnityEvent confirmAction;

	void Start(){
		//doAction.AddListener(() => {Debug.Log("Action");});
	}

	void Update () {
        if(Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor){
			if(Input.GetKeyDown(KeyCode.Space))
				doAction.Invoke();
			else if(Input.GetKeyDown(KeyCode.Return))
				confirmAction.Invoke();
		}
		else{
			for(int i = 0; i < Input.touchCount; i++){
				if(Input.touches[i].phase == TouchPhase.Began)
					doAction.Invoke();
			}
		}
	}

}
