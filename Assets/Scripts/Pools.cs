﻿using UnityEngine;
using System.Collections;

public class Pools : MonoBehaviour {

    public PoolController roadTilePoolController;
	public PoolController obstaclePoolController;
	public PoolController coinPoolController;

    public static PoolController RoadTilePoolController;
	public static PoolController ObstaclePoolController;
	public static PoolController CoinPoolController;

	void Awake(){
		RoadTilePoolController = roadTilePoolController;
		ObstaclePoolController = obstaclePoolController;
		CoinPoolController = coinPoolController;
	}
}
