﻿using UnityEngine;
using System.Collections;

public class CrawlingController : MonoBehaviour {

	public float maxSpeed = 4f;

    private float curSpeed = 0f;
    private bool shouldCrawl = false;

	void Update () {
		if(shouldCrawl){
            curSpeed = Mathf.Lerp(curSpeed, maxSpeed, 10f * Time.deltaTime);
		}
		else{
			curSpeed = Mathf.Lerp(curSpeed, 0f, 10f * Time.deltaTime);
		}

		transform.position += Vector3.forward * curSpeed * Time.deltaTime;
	}

    public void StartCrawling () {
        curSpeed = maxSpeed;
        shouldCrawl = true;
    }

    public void EndCrawling () {
        shouldCrawl = false;
    }
}
