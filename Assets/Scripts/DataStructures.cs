﻿using System.Collections;

public enum BranchSide {Empty = 0, Obstacle, Coin};
public enum Direction {Forward = 0, Back, Left, Right, Up, Down};
