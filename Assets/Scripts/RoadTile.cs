﻿using UnityEngine;
using System.Collections;

public class RoadTile : MonoBehaviour {

	[HideInInspector]
	public int tileNumber = 0;
	public int rotIndex = 0;

	private float rotAngle = 0f;
	private float rotSpeed = 20f;

    public Transform[] possibleSpawnPoints;

    public bool isBranch = true;
    public Direction direction;

	void Update () {
		rotAngle = rotIndex * 90f;

        if(transform.localEulerAngles.z != rotAngle){
            transform.localEulerAngles = Vector3.forward * Mathf.LerpAngle(transform.localEulerAngles.z, rotAngle, rotSpeed * Time.deltaTime);
		}
	}

	public void Rotate(){
		rotIndex++;

		if(rotIndex > 3)
			rotIndex = 0;
	}

	public void Disassamble(){
		Invoke("DisassambleAfterDelay", 0.5f);
	}

	void DisassambleAfterDelay(){
        for(int i = transform.childCount - 1; i >= 0; i--){
            Transform t = transform.GetChild(i);
            switch(t.tag){
                case "Coin":
                    Pools.CoinPoolController.AddTransform(t);
                    break;
                case "Obstacle":
                    Pools.ObstaclePoolController.AddTransform(t);
				    break;
    			default:
    				break;
			}
		}

        if (isBranch)
        {
            Pools.RoadTilePoolController.AddTransform(transform);
        } else
        {
            Pools.RoadTilePoolController.AddTransform(transform);
        }
	}

    public Transform GetNextTileSpawnPoint(){
        return possibleSpawnPoints[Random.Range(0, possibleSpawnPoints.Length)];
    }
}

