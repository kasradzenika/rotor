﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public RoadController roadController;
    public GameObject deathPool;

    void OnTriggerEnter(Collider col)
    {
        switch (col.tag)
        {
            case "RoadTile":
                roadController.RemovePreviousTile();
                roadController.BuildNextTile();
                break;
            case "Coin":
                break;
            case "Obstacle":
                Instantiate(deathPool, transform.position, Quaternion.identity);
                break;
            default:
                break;
        }
    }   
}
