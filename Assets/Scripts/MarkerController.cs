﻿using UnityEngine;
using System.Collections;

public class MarkerController : MonoBehaviour {

    public RoadController roadController;
    public float speed = 50f;

    [HideInInspector]
    public Transform target;

	
	void Update () {
        if (target != null)
        {
            transform.position = Vector3.Lerp(transform.position, target.position, speed * Time.deltaTime);
        }
	}
}
