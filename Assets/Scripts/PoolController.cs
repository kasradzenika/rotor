﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


/**
 * retrieve and return child objects
 */

public class PoolController : MonoBehaviour {

	//this list is maintained at all times, even after the transform is
	//dispatched via GetRandomTransform or GetTransformWithName or returned
	public bool shouldKeepChildrenDisabled = true;
	private Transform[] _transforms;

	public Transform[] transforms{
		get{
			return _transforms;
		}
	}

	public void Awake(){
		//initialize the transforms
		_transforms = new Transform[transform.childCount];

		for (int i=0; i<transform.childCount; i++)
			_transforms [i] = transform.GetChild (i);
	}

	/// <summary>
	/// Gets the random transform, if there are no children returns NULL!
	/// </summary>
	/// <returns>The random child transform</returns>
	public Transform GetRandomTransform(){
		if (transform.childCount > 0) {
			Transform result = transform.GetChild (Random.Range (0, transform.childCount));
			result.gameObject.SetActive(true);
			result.parent = null;

			return result;
		}

		return null;
	}

	/// <summary>
	/// Returns a child with the specified name, if child name is not found reutrns NULL!
	/// </summary>
	/// <returns>The child transform with name</returns>
	/// <param name="name">Name</param>
	public Transform GetTransformWithName(string name){
		List<int> indexes = new List<int>();

		for (int i=0; i<transform.childCount; i++) {
			if(transform.GetChild(i).name == name){
				indexes.Add(i);
			}
		}



		Transform theChosenOne = null;
		if (indexes.Count > 0) {
			theChosenOne = transform.GetChild(indexes[Random.Range(0, indexes.Count)]);
		}

		if (theChosenOne != null) {
			theChosenOne.gameObject.SetActive (true);
			theChosenOne.parent = null;
		}
		
		return theChosenOne;
	}

	public void AddTransform(Transform _transform){
		_transform.position = transform.position;
		_transform.SetParent (transform);

		if (shouldKeepChildrenDisabled) {
			_transform.gameObject.SetActive (false);
		}
	}
}
