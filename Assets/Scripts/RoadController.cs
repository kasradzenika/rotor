﻿using UnityEngine;
using System.Collections.Generic;

public class RoadController : MonoBehaviour {

    //concept:
    //all the new tiles are rotating, only the first rotor is stopping on tap :/

	public List<RoadTile> roadTiles;
	public RoadConstructor roadConstructor;

	public void AddTile(RoadTile roadTile){
		roadTile.transform.parent = transform;
		roadTiles.Add(roadTile);
	}

	public void RemovePreviousTile(){
		roadTiles[0].Disassamble();
		roadTiles.RemoveAt(0);
	}

	public void BuildNextTile(){
		roadConstructor.BuildNextBranch();
	}

    public RoadTile GetActiveTile(){
        return roadTiles [roadTiles.Count - 1];
    }

	public void RotateActiveTile(){
        for (int i = 0; i < roadTiles.Count; i++)
        {
            roadTiles [i].Rotate();
        }
	}
}
