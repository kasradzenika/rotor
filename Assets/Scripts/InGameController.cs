﻿using UnityEngine;
using System.Collections;

public class InGameController : MonoBehaviour {

    public CrawlingController crawlingController;
    public RoadConstructor roadConstructor;

    private bool gameIsRunning = false;

    //the in-game logic starts here
    public void StartGame(){
        if (!gameIsRunning)
        {
            crawlingController.StartCrawling();
            roadConstructor.BuildInitialTiles();

            gameIsRunning = true;
        }
    }

    //in-game must call this in the end
    public void EndGame(){
        //TODO
        if (gameIsRunning)
        {
            gameIsRunning = false;
        }
    }
}
