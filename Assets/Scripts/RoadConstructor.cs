﻿using UnityEngine;
using System.Collections;

public class RoadConstructor : MonoBehaviour
{
    public RoadController roadController;

    [Tooltip("x: probability, [0,1] random value \ny: num of obstacles on a tile")]
    public AnimationCurve obstacleDistribution;
    public float coinProbability = 0.2f;

    public int initialNumOfTiles = 5;

    private int numOfTiles = 0;
    private Direction curBuildDirection;
    private Vector3 lastRoadPiecePos;

    void Start(){
        curBuildDirection = Direction.Right;
    }

    public void BuildInitialTiles()
    {
        //the first tiles don't have obstacles or coins
        BranchSide[] initialBranchSides = GenerateEmptyBranchSides();

        for (int t = 0; t < initialNumOfTiles; t++)
        {
            BuildNextBranch(initialBranchSides);
        }
    }

    public void BuildNextBranch()
    {
        //concept: each tile can have maximum of 2 obstacles
        //... obstacle locations does NOT depend on previous tile (TODO)

        BranchSide[] sides = GenerateEmptyBranchSides();
        int numOfObstacles = (int)obstacleDistribution.Evaluate(Random.value);
        int[] indexesOfSidesToModify = new int[numOfObstacles];

        //set obstacles
        for (int i = 0; i < indexesOfSidesToModify.Length; i++)
        {
            int randomIndex;
            do
            {
                randomIndex = Random.Range(0, 4);
            } while (sides [randomIndex] != BranchSide.Empty);
            sides [randomIndex] = BranchSide.Obstacle;
        }

        //set coin
        if (Random.value < coinProbability)
        {
            int randomIndex;
            do
            {
                randomIndex = Random.Range(0, 4);
            } while (sides [randomIndex] != BranchSide.Empty);
            sides [randomIndex] = BranchSide.Coin;
        }

        //build
        BuildNextBranch(sides);
    }

    //each tile has 4 sides, side is type of TileSide
    public void BuildNextBranch(BranchSide[] sides)
    {
        Transform tile = Pools.RoadTilePoolController.GetRandomTransform();

        //s = side
        for (int s = 0; s < 4; s++)
        {
            Transform branchSide = null;

            switch (sides [s])
            {
                case BranchSide.Empty:
				    //tileSide is empty
                    break;
                case BranchSide.Coin:
                    branchSide = Pools.CoinPoolController.GetRandomTransform();
                    break;
                case BranchSide.Obstacle:
                    branchSide = Pools.ObstaclePoolController.GetRandomTransform();
                    break;
                default:
                    break;
            }

            if (branchSide != null)
            {
                branchSide.eulerAngles = Vector3.forward * 90f * s;
                branchSide.parent = tile;
            }
        }

        Transform nextTileInfo = roadController.GetActiveTile().GetNextTileSpawnPoint();
        tile.position = nextTileInfo.position;
        tile.rotation = nextTileInfo.rotation;

        RoadTile roadTileComponent = tile.GetComponent<RoadTile>();
        roadTileComponent.rotIndex = roadController.GetActiveTile().rotIndex;
        roadTileComponent.tileNumber = numOfTiles;
        roadController.AddTile(roadTileComponent);

        numOfTiles++;
    }


    //just a helper func
    private BranchSide[] GenerateEmptyBranchSides()
    {
        return new BranchSide[4]
        {
            BranchSide.Empty,
            BranchSide.Empty,
            BranchSide.Empty,
            BranchSide.Empty
        };
    }
}
